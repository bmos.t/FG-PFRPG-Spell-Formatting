[![Build FG-Usable File](https://github.com/bmos/FG-PFRPG-Spell-Formatting/actions/workflows/create-ext.yml/badge.svg)](https://github.com/bmos/FG-PFRPG-Spell-Formatting/actions/workflows/create-ext.yml) [![Luacheck](https://github.com/bmos/FG-PFRPG-Spell-Formatting/actions/workflows/luacheck.yml/badge.svg)](https://github.com/bmos/FG-PFRPG-Spell-Formatting/actions/workflows/luacheck.yml)

# Spell Formatting
This extension modifies the character-specific copies of spells (accessed via their actions tab) to allow fully-formatted descriptions including italics, tables (detect magic is a great example), links, etc. At this time, you need to drag the spells into actions after installing this extension for the special formatting to be included.

# Compatibility
This extension has been tested with [FantasyGrounds Unity](https://www.fantasygrounds.com/home/FantasyGroundsUnity.php) 4.1.13 (2022-01-05).

# Features
* Spells on the actions tab will display with formatting when opened by clicking the link button.
* Changes to the fully-formatted description will be saved to the plain-text description as well so that they will not be lost if you stop using this extension.

# Example Image
<img src="https://i.imgur.com/Hm0LFi7.png" alt="spell formatting comparison"/>
